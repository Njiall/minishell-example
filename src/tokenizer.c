#include "tokens.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

// =============================================================================
//                  States & definitions
// =============================================================================

// All the different states of the machine
// Consider that there is a table of TS_LEN ^ 2 possibilities.
// Most of them are incorrect in our case.
typedef enum {
  // Redef of mandatory default values
  _TS_NONE = TS_NONE,
  _TS_INIT = TS_INIT,
  _TS_END = TS_END,

  // Normal words
  TS_WORD = TS_SAFE_RANGE,
  // Separators
  TS_SPACE,
  // $name
  TS_VAR,
  TS_IDVAR,
  // cmd;
  TS_SCOL,
  // OPs
  TS_PIPE,
  // Redirictions
  TS_RREDIR,        // >
  TS_LREDIR,        // <
  TS_RREDIR_APPEND, // >>
  TS_LREDIR_APPEND, // <<
  TS_REDIR_RW,      // <>
  // Strings
  TS_S_STR_IN,
  TS_S_STR, // 'str'
  TS_S_STR_OUT,
  TS_D_STR_IN,
  TS_D_STR, // "str"
  TS_D_STR_OUT,

  // Boundary
  TS_LEN,
} _t_tok_state;
#define TS_ANY TS_INIT... TS_LEN - 1

// These are the different classes of chars we can use inside the tokenizer.
typedef enum {
  TC_NONE,
  TC_END,

  TC_SPACE,
  TC_LETTER,
  TC_NUMBER,
  TC_S_QUOTE,
  TC_D_QUOTE,
  TC_PIPE,
  TC_RCHEV,
  TC_LCHEV,
  TC_DOLLAR,
  TC_SCOL,

  TC_LEN,
} t_tok_class;
#define TC_ANY TC_NONE + 1 ... TC_LEN - 1

// We use this to convert from a char to an enum value we can use in our tables.
// This takes a given char and gives us an id to know what kind of value
// was the input without having to care exactly what.
// So you can say it compresses our input:
//  .-----------.        .-----------.       .-- - -
//  |           |        |char       |       |
//  |   Input   | =====> |   Class   |  ...  |  ...
//  |     (char)|        |           |       |
//  `-----------'        `-----------'       `-- - -
t_tok_class compress[1 << 8] = {
    // Used for words
    ['a' ... 'z'] = TC_LETTER,
    ['A' ... 'Z'] = TC_LETTER,
    ['_'] = TC_LETTER,
    ['-'] = TC_LETTER,
    ['/'] = TC_LETTER,
    ['?'] = TC_LETTER,
    // Used for words and redirections
    ['0' ... '9'] = TC_NUMBER,

    // Used as separator
    [' '] = TC_SPACE,
    ['\t'] = TC_SPACE,
    ['\n'] = TC_SPACE,
    // Operators
    ['>'] = TC_RCHEV,
    ['<'] = TC_LCHEV,
    ['|'] = TC_PIPE,
    [';'] = TC_SCOL,
    ['$'] = TC_DOLLAR,
    ['\''] = TC_S_QUOTE,
    ['\"'] = TC_D_QUOTE,

    // End
    ['\0'] = TC_END,
};

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wenum-conversion"
// This dicatates the 'truth table' of the tokenizer, it's the transition
// table of our automata: https://en.wikipedia.org/wiki/Finite-state_machine
// Check this out for more complete view on how this works in principle.
//
// Here is the simplified view:
//
// Our machine consumes the input to produce tokens, doing this chages the
// internal current state of the machine, change that we can detect and act
// up making us capable to create token in a specific state of the machine.
//
// Machine:
//                 Input (char)
//                   |
//  .-----------.    |   .-----------.       .-- - -
//  |prev       |    v   |next       |       |
//  |   State   | =====> |   State   |  ...  |  ...
//  |           |        |           |       |
//  `-----------'    v   `-----------'       `-- - -
//                   |
//                   '------> handlers[prev][next] ---> Token
//
// This seems like a wierd way to do it but remember:
// The order we define the states here matters as there are some
// overrites are occuring.
// So the last asignation has a bigger priority over the first.
t_tok_state state[TS_LEN][TC_LEN] = {
    // Word handle
    [TS_ANY][TC_LETTER] = TS_WORD, // Needs to change the any to handle strings
    [TS_ANY][TC_NUMBER] = TS_WORD,
    [TS_ANY][TC_SPACE] = TS_SPACE,

    // Variables
    [TS_ANY][TC_DOLLAR] = TS_VAR,
    [TS_VAR][TC_LETTER] = TS_IDVAR,
    [TS_VAR][TC_NUMBER] = TS_IDVAR,
    [TS_IDVAR][TC_LETTER] = TS_IDVAR,
    [TS_IDVAR][TC_NUMBER] = TS_IDVAR,

    [TS_ANY][TC_SCOL] = TS_SCOL,

    // Redirections
    // left redirs
    [TS_ANY][TC_LCHEV] = TS_LREDIR, // ? dunno if this is really valid
    // Append
    [TS_LREDIR][TC_LCHEV] = TS_LREDIR_APPEND,
    // right redirs
    [TS_ANY][TC_RCHEV] = TS_RREDIR, // ? dunno if this is really valid
    // Append
    [TS_RREDIR][TC_RCHEV] = TS_RREDIR_APPEND,
    // Open close
    [TS_LREDIR][TC_RCHEV] = TS_REDIR_RW,
    // This should error since we have no variable name
    [TS_VAR][TC_LCHEV] = TS_NONE,
    [TS_VAR][TC_RCHEV] = TS_NONE,

    // Pipes
    [TS_ANY][TC_PIPE] = TS_PIPE,
    // || shouldn't work
    [TS_PIPE][TC_PIPE] = TS_NONE,

    // Strs
    // Start of string
    [TS_ANY][TC_S_QUOTE] = TS_S_STR_IN,
    // In string
    [TS_S_STR_IN][TC_ANY] = TS_S_STR,
    [TS_S_STR][TC_ANY] = TS_S_STR,
    // End of string
    [TS_S_STR][TC_S_QUOTE] = TS_S_STR_OUT,
    [TS_S_STR_IN][TC_S_QUOTE] = TS_S_STR_OUT,
    // Start of string
    [TS_ANY][TC_D_QUOTE] = TS_D_STR_IN,
    // In string
    [TS_D_STR_IN][TC_ANY] = TS_D_STR,
    [TS_D_STR][TC_ANY] = TS_D_STR,
    // End of string
    [TS_D_STR][TC_D_QUOTE] = TS_D_STR_OUT,
    [TS_D_STR_IN][TC_D_QUOTE] = TS_D_STR_OUT,

    // Obliged end do not override
    [TS_ANY][TC_END] = TS_END,
};

// =============================================================================
//                  Handlers
// =============================================================================

bool drop_anchor(t_tok_stream *ctx, char **data, t_token *token) {
  // printf("anchor delivery!\n");
  *data = ctx->input;
  return false;
}

bool token_word(t_tok_stream *ctx, char **data, t_token *token) {
  *token = (t_token){
      .type = TOK_WORD,
      .text = *data,
      .len = ctx->input - *data,
  };
  // printf("word\n");
  return true;
}

bool token_redir(t_tok_stream *ctx, char **data, t_token *token) {
  char *fmt[REDIR_LEN] = {
      [RREDIR] = ">",         [RREDIR_APPEND] = ">>", [LREDIR] = "<",
      [LREDIR_APPEND] = "<<", [REDIR_RW] = "<>",
  };

  t_redir_type type;

  // Parse redir type
  switch (ctx->next) {
  case TS_LREDIR:
    type = LREDIR;
    break;
  case TS_RREDIR:
    type = RREDIR;
    break;
  case TS_LREDIR_APPEND:
    type = LREDIR_APPEND;
    break;
  case TS_RREDIR_APPEND:
    type = RREDIR_APPEND;
    break;
  case TS_REDIR_RW:
    type = REDIR_RW;
    break;
  default:
    type = REDIR_ERROR;
    break;
  }

  *token = (t_token){
      .redir =
          {
              .type = TOK_REDIR,
              .text = *data,
              .len = ctx->input - *data,
              .op = type,
          },
  };
  // printf("redir\n");
  return true;
}

bool token_pipe(t_tok_stream *ctx, char **data, t_token *token) {
  *token = (t_token){
      .type = TOK_PIPE,
      .text = ctx->input - 1,
      .len = 1,
  };
  // printf("pipe\n");
  return true;
}

bool token_str(t_tok_stream *ctx, char **data, t_token *token) {
  *token = (t_token){
      .type = TOK_STR,
      .text = *data,
      .len = ctx->input - *data,
  };
  // printf("str\n");
  return true;
}

bool token_variable(t_tok_stream *ctx, char **data, t_token *token) {
  *token = (t_token){
      .type = TOK_VARIABLE,
      .text = *data,
      .len = ctx->input - *data,
  };
  // printf("var\n");
  return true;
}

bool token_scol(t_tok_stream *ctx, char **data, t_token *token) {
  *token = (t_token){
      .type = TOK_SCOL,
      .text = ctx->input - 1,
      .len = 1,
  };
  // printf(";\n");
  return true;
}

// The hanlder table used to generate tokens from the input we get.
bool (*handler[TS_LEN][TS_LEN][3])(t_tok_stream *, char **, t_token *) = {
    // Default case
    [TS_ANY][TS_WORD] = {&drop_anchor},
    // Word validation
    [TS_WORD][TS_ANY] = {&token_word},
    [TS_WORD][TS_END] = {&token_word},
    [TS_WORD][TS_PIPE] = {&token_word, &drop_anchor},
    // Exception when we already are in a word
    [TS_WORD][TS_WORD] = {},

    // Variables
    [TS_VAR][TS_IDVAR] = {&drop_anchor},
    // We are at the end
    [TS_IDVAR][TS_ANY] = {&token_variable, &drop_anchor},
    [TS_IDVAR][TS_END] = {&token_variable},
    // We are inside the variable
    [TS_IDVAR][TS_IDVAR] = {},

    // Redir start for word bounding
    [TS_ANY][TS_LREDIR] = {&drop_anchor},
    [TS_ANY][TS_RREDIR] = {&drop_anchor},
    [TS_IDVAR][TS_LREDIR] = {&token_variable, &drop_anchor},
    [TS_IDVAR][TS_RREDIR] = {&token_variable, &drop_anchor},
    // Don't forget to end the word
    [TS_WORD][TS_REDIR_RW] = {&token_word, &drop_anchor},
    [TS_WORD][TS_LREDIR] = {&token_word, &drop_anchor},
    [TS_WORD][TS_LREDIR_APPEND] = {&token_word, &drop_anchor},
    [TS_WORD][TS_RREDIR] = {&token_word, &drop_anchor},
    [TS_WORD][TS_RREDIR_APPEND] = {&token_word, &drop_anchor},
    // End the token
    [TS_LREDIR][TS_ANY] = {&token_redir, &drop_anchor},
    [TS_LREDIR_APPEND][TS_ANY] = {&token_redir, &drop_anchor},
    [TS_RREDIR][TS_ANY] = {&token_redir, &drop_anchor},
    [TS_RREDIR_APPEND][TS_ANY] = {&token_redir, &drop_anchor},
    [TS_REDIR_RW][TS_ANY] = {&token_redir, &drop_anchor},
    // Special case on end
    [TS_LREDIR][TS_END] = {&token_redir},
    [TS_LREDIR_APPEND][TS_END] = {&token_redir},
    [TS_RREDIR][TS_END] = {&token_redir},
    [TS_RREDIR_APPEND][TS_END] = {&token_redir},
    [TS_REDIR_RW][TS_END] = {&token_redir},
    // Exception when we are already in the redir
    [TS_LREDIR][TS_LREDIR_APPEND] = {},
    [TS_RREDIR][TS_RREDIR_APPEND] = {},
    [TS_LREDIR][TS_REDIR_RW] = {},

    // Pipe
    [TS_PIPE][TS_ANY] = {&token_pipe},
    [TS_PIPE][TS_END] = {&token_pipe},
    [TS_PIPE][TS_WORD] = {&token_pipe, &drop_anchor},

    // Scol ;
    [TS_SCOL][TS_ANY] = {&token_scol, &drop_anchor},
    [TS_SCOL][TS_END] = {&token_scol},
    // Str
    [TS_S_STR_IN][TS_S_STR] = {&drop_anchor},
    [TS_S_STR_IN][TS_S_STR_OUT] = {&drop_anchor, &token_str}, // Empty string
    [TS_S_STR][TS_S_STR_OUT] = {&token_str},
    [TS_D_STR_IN][TS_D_STR] = {&drop_anchor},
    [TS_D_STR_IN][TS_D_STR_OUT] = {&drop_anchor, &token_str}, // Empty string
    [TS_D_STR][TS_D_STR_OUT] = {&token_str},
};
#pragma clang diagnostic pop

// =============================================================================
//                  Machine
// =============================================================================

#include <stdio.h>
t_token tokenize(t_tok_stream *iter) {
  while (iter->next != TS_END && iter->next != TS_NONE) {
    // Gets the new state from the previous state and the current char.
    iter->next = state[iter->prev][compress[(uint8_t)*iter->input]];

    // printf("%02d %02d [%c]\n", iter->prev, iter->next, *iter->input);

    // The token to be returned, note that it is passed to the handler
    // function in order, so if the the first function sets the token,
    // the 2nd function gets it as an argument so you can do conditional
    // rewriting of the token for some obscure reason. (We won't do it anyway).
    // Note that we can also return error tokens so we can make an error handler
    // and use it to make a custom error with all the data needed to
    // print something pretty.
    t_token token;
    // Did we generate a token
    bool gen = false;
    if (handler[iter->prev][iter->next])
      // Iterates over the handlers function pointers
      // Loops on all the data but stops when NULL handler is found
      for (size_t i = 0; i < sizeof(**handler) / sizeof(***handler) &&
                         handler[iter->prev][iter->next][i];
           i++)
        if (handler[iter->prev][iter->next][i](iter, &iter->data, &token))
          gen = true;
    // Update machine state and input
    iter->prev = iter->next;
    iter->input++;
    // If we generated a token we stop
    if (gen)
      return token;
  }

  // Default token if we ended the machine
  if (iter->next == TS_END)
    return (t_token){
        .type = TOK_END,
    };
  // Else we generate an error, there must be a syntax error
  return (t_token){
      .type = TOK_NONE,
      .text = iter->input,
      .len = 1,
  };
}