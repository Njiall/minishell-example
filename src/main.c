#include "command.h"
#include "env.h"
#include "minishell.h"
#include "parser.h"
#include "tokens.h"

#include <readline/history.h>
#include <readline/readline.h>

t_shell shell = (t_shell){
    // Assign compilation constants
    .prompt = "\e[35m❯ \e[0m",
};

t_status exec(t_cmd cmd) { return (t_status){}; }

int main(int c, char **v, char **env) {
  t_status status = (t_status){};
  // Parse env
  // load_env(&g_shell, env);

  // Use readline history
  using_history();

  // Input from readline
  char *input;
  while ((input = readline(shell.prompt))) {
    // Parse the command
    t_cmd cmd; // This is mutable and modified by parse
    if (!(status = parse(input, &cmd)).valid) {
      // The command couln't parse
    }

    // Then exec command
    if (!(status = exec(cmd)).valid) {
      // There was something wrong with the exec
    }

    // Command was successfull, we add to history
    add_history(input);
    // Free user input from readline
    free(input);
  }

  return (status.code);
}
