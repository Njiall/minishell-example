#include "parser.h"
#include "command.h"
#include "dynarray.h"
#include "minishell.h"
#include "tokens.h"

#define DEFAULT_TOK_STREAM(in)                                                 \
  (t_tok_stream) { .input = in, .prev = TS_INIT, .next = TS_INIT, }

char *fmt[TOK_LEN] = {
    [TOK_END] = "\e[1;32mPIPE\e[0m",

    [TOK_WORD] = "\e[1;34mWORD\e[0m",   [TOK_ASSIGN] = "\e[1;34mASSIGN\e[0m",
    [TOK_STR] = "\e[1;35mSTR\e[0m",     [TOK_PIPE] = "\e[1;33mPIPE\e[0m",
    [TOK_REDIR] = "\e[1;33mREDIR\e[0m", [TOK_VARIABLE] = "\e[1;34mVAR\e[0m",
    [TOK_SCOL] = "\e[1;33mSCOL\e[0m",
};

void print_token(t_token *tok) {
  switch (tok->type) {
  case TOK_NONE:
    printf("\e[1;31mError\e[0m\n");
    break;
  case TOK_END:
    printf("\e[1;32mEnd\e[0m\n");
    break;
  default:
    printf("\e[1;34mToken\e[0m | type: %s, len: %zu, text: '\e[1m%.*s\e[0m'\n",
           fmt[tok->type], tok->len, (int)tok->len, tok->text);
  }
}

t_status parse(char *input, t_cmd *cmd) {
  // Stack to use for the parsing of the command
  t_dynarray stack = ft_dynarray_create_loc(0, 0);

  // Token to use as a token stream
  t_token token;
  // Token stream context
  t_tok_stream ctx = DEFAULT_TOK_STREAM(input);

  // Token stream
  while ((token = tokenize(&ctx)).type != TOK_END && token.type != TOK_NONE) {
    print_token(&token);
    // Define the syntax to parse here
  }
  // Print last token
  print_token(&token);

  // Free the custom stack
  ft_dynarray_destroy(&stack, false);
}
