#ifndef DYNSTRING_H
#define DYNSTRING_H

#include <stddef.h>
#include <stdbool.h>

typedef struct	s_dynstring_internal {
	size_t		size;
	// Pointer to what is after the size
	// This should be the pointer returned to the user.
	char		string[0];
}				t_dynstring_internal;

// Is strictly a `dynstring`
typedef char *	t_dynstring;
// Can be a `dynstring` or a `C string`
typedef char *	t_dynstring_any;

// Creation

// Create an empty string of size 0
t_dynstring		dynstring_empty(void);
// Create an empty string of size `len`
t_dynstring		dynstring_create(
		const size_t len
);
// Duplicate a string
t_dynstring		dynstring_duplicate(
		const t_dynstring string
);
// Create an string from a C null terminated
t_dynstring		dynstring_from(
		const t_dynstring_any string
);
// Create an string from a C null terminated of size `len` < size of string
// otherwise it's undefined behavior
t_dynstring		dynstring_from_len(
		const char * const string,
		const size_t len
);

// Free a string
void			dynstring_free(
		const t_dynstring string
);

// Manipulation

// This consumes both the strings to make the returned one.
t_dynstring		dynstring_concat(
		t_dynstring a,
		t_dynstring b
);
// This consumes the first strin to make the returned one.
t_dynstring		dynstring_join(
		t_dynstring a,
		t_dynstring b
);
// Inserts a string inside an other
t_dynstring		dynstring_insert(
		t_dynstring dst,
		t_dynstring src,
		size_t index
);
t_dynstring		dynstring_cut(
		t_dynstring src,
		size_t index,
		size_t length
);

// Utils

// 
int			dynstring_compare(
		t_dynstring a,
		t_dynstring b
);
// 
bool		dynstring_equal(
		t_dynstring a,
		t_dynstring b
);
// 
bool		dynstring_start_with(
		t_dynstring src,
		t_dynstring start
);
t_dynstring	dynstring_map(
		t_dynstring src,
		struct {
			const char		*ptr;
			const size_t	size;
		} (*mapper)(const char * const ptr)
);

#endif
