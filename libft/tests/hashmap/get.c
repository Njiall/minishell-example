#include <string.h>
#include <criterion/criterion.h>
#include <criterion/parameterized.h>
#include <criterion/logging.h>

#include "hashmap.h"
#include "ft_tests.h"

typedef struct			s_entry {
	const uint8_t		key[256];
	const size_t		len;
	const size_t		data;
}						t_entry;

typedef struct			s_test {
	// Display
	char				desc[4096];

	// Input
	t_entry				entries[64];
	size_t				elen;

	// Outputs
	t_entry				out;
	t_hash_function_id	id;

	bool				invalid;
}						t_test;

ParameterizedTestParameters(hashmap, get_strings) {
	static t_test			tests[] = {
		(t_test){
			.desc = "Get invalid with no entries",
			.id = HASH_FUNC_DJB2A,

			.out = (t_entry){
				.key = "This an example",
				.len = 15,
				.data = 0x42,
			},
			.invalid = true,
		},
		(t_test){
			.desc = "Get invalid with entries",
			.id = HASH_FUNC_DJB2A,

			.entries = {
				(t_entry){
					.key = "This not the example 1",
					.len = 22,
					.data = 0x55,
				},
				(t_entry){
					.key = "This not the example 2",
					.len = 22,
					.data = 0x55,
				},
				(t_entry){
					.key = "This not the example 3",
					.len = 22,
					.data = 0x55,
				},
				(t_entry){
					.key = "This not the example 4",
					.len = 22,
					.data = 0x55,
				},
				(t_entry){
					.key = "This not the example 5",
					.len = 22,
					.data = 0x55,
				},
			},
			.elen = 4,

			.out = (t_entry){
				.key = "This an example",
				.len = 15,
				.data = 0x42,
			},
			.invalid = true,
		},
		(t_test){
			.desc = "Get valid with entries",
			.id = HASH_FUNC_DJB2A,

			.entries = {
				(t_entry){
					.key = "This an example",
					.len = 15,
					.data = 0x42,
				},
				(t_entry){
					.key = "This not the example 1",
					.len = 22,
					.data = 0x55,
				},
				(t_entry){
					.key = "This not the example 2",
					.len = 22,
					.data = 0x55,
				},
				(t_entry){
					.key = "This not the example 3",
					.len = 22,
					.data = 0x55,
				},
			},
			.elen = 4,

			.out = (t_entry){
				.key = "This an example",
				.len = 15,
				.data = 0x42,
			},
			.invalid = false,
		},
	};

	// generate parameter set
	return cr_make_param_array(t_test, tests, sizeof(tests) / sizeof(t_test));
}

ParameterizedTest(t_test *test, hashmap, get_strings)
{
	// Init
	t_hashmap		map = ft_hashmap_create_id_string_loc(
			test->id
	);
	// Fills contents
	for (size_t i = 0; i < test->elen; i++) {
		ft_hashmap_set(
				&map,
				test->entries[i].key,
				test->entries[i].len,
				(const void * const)test->entries[i].data
		);
	}

	// Run test
	size_t			value;
	bool got_result = ft_hashmap_get(
			&map,
			test->out.key,
			test->out.len,
			(const void**)&value
	);
	bool match_result = true;
	if (got_result) {
		match_result = (test->out.data == value);
	}

	// Results
	cr_expect(got_result == !test->invalid, "%s\nResult of operation doesn't match.", test->desc);
	cr_expect(match_result, "%s\nValue of operation doesn't match.\n%s", test->desc, get_mem_diff(
				&value,
				&test->out.data,
				sizeof(value),
				sizeof(test->out.data)
	));

	// Cleanup
	ft_hashmap_destroy_loc(&map);
}
