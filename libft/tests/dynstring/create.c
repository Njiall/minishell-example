#include <string.h>
#include <criterion/criterion.h>
#include <criterion/parameterized.h>
#include <criterion/logging.h>

#include "dynstring.h"

typedef struct {
	size_t		len;
}				t_test;

ParameterizedTestParameters(dynstring, create) {
	static t_test			tests[] = {
		(t_test){0},
		(t_test){10},
		(t_test){1},
		(t_test){42},
	};

	return cr_make_param_array(t_test, tests, sizeof(tests) / sizeof(t_test));
}

ParameterizedTest(t_test *test, dynstring, create)
{
	t_dynstring	new = dynstring_create(test->len);

	for (size_t i = 0; i < test->len; ++i) {
		char addr = new[i];
	}
	cr_expect(new[0] == '\0',
			"Incorrect init of string"
	);
}
