#include <string.h>
#include <criterion/criterion.h>
#include <criterion/parameterized.h>
#include <criterion/logging.h>

#include "dynstring.h"

typedef struct s_test {
	char		str[4096];

}				t_test;

ParameterizedTestParameters(dynstring, from) {
	static t_test			tests[] = {
		(t_test){.str = "wow"},
		(t_test){.str = "lol"},
		(t_test){.str = "xd"},
		(t_test){.str = ""},
	};

	// generate parameter set
	return cr_make_param_array(t_test, tests, sizeof(tests) / sizeof(t_test));
}

ParameterizedTest(t_test *test, dynstring, from)
{
	t_dynstring	new = dynstring_from(test->str);

	cr_expect(!strcmp(new, test->str),
			"Incorrect init of string"
	);
}
