/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/21 01:06:13 by mbeilles          #+#    #+#             */
/*   Updated: 2019/08/27 13:51:04 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void				*ft_realloc(void *buff, size_t size)
{
	char		*tmp;

	if ((tmp = (char*)malloc(size)) == NULL)
		return (NULL);
	if ((char*)buff)
	{
		ft_memcpy(tmp, (char*)buff, size);
		free(buff);
	}
	return (tmp);
}
