/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dynarray_queue.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/24 06:08:27 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/27 15:09:51 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "dynarray.h"

/*
** /!\ This utility function shouln't be used outside the lib /!\
**
** Calculates and reallocate the array if the new element cannot fit in it.
*/
static inline t_dynarray*ft_dynarray_fits_resize(
		t_dynarray *arr,
		const uint64_t size
)
{
	uint8_t				*new;

	if (arr->size <= arr->index + arr->offset + size)
	{
		while (arr->size <= arr->index + arr->offset + size)
			arr->size <<= 1u;
		if (!(arr->array = realloc(arr->array, arr->size)))
			ft_exit(1, ft_strajoin(4, __FILE__, ": ", __func__,
						": No space left on device.\n"), NULL);
	}
	return (arr);
}

/*
** Pushes an element at the end of the queue
**
** Returns the element index in the array
*/
void					*ft_dynarray_queue(
		t_dynarray *arr,
		const void * const elem,
		const uint64_t size
)
{
	return (ft_dynarray_push(arr, elem, size));
}

/*
** Pops the first element out of a dynarray.
** 		It stores it in the copy parameter and asumes the copy location can
** 		stores the data since allocating is a bad idea (performance wise).
**
** Returns if an element has been fetched.
*/
bool					ft_dynarray_unqueue(
		t_dynarray *arr,
		void * const cpy,
		const uint64_t size
)
{
	if (arr->index < size)
		return (false);
	memcpy(cpy, arr->array + arr->offset, size);
	arr->offset += size;
	arr->index -= size;
	return (true);
}

void					*ft_dynarray_queue_top(
		t_dynarray *arr,
		const uint64_t size
)
{
	if (arr->index)
		return (arr->array + arr->offset);
	return (NULL);
}

void					*ft_dynarray_queue_bottom(
		t_dynarray *arr,
		const uint64_t size
)
{
	if (size <= arr->index)
		return (arr->array + arr->offset + arr->index - size);
	return (NULL);
}
