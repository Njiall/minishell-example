/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dynarray_remove.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/31 05:05:03 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/28 17:24:11 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "dynarray.h"
#include "libft.h"

bool				ft_dynarray_yank(
		t_dynarray *arr,
		const uint64_t index,
		void * const cpy,
		const uint64_t size
)
{
	if (arr->index < index + size)
		return (false);
	memcpy(cpy, arr->array + arr->offset + arr->index - size, size);
	return (ft_dynarray_remove(arr, index, size));
}

bool				ft_dynarray_remove(
		t_dynarray *arr,
		const uint64_t index,
		const uint64_t size
)
{
	// Empty and out of bound fails imidiatly
	if (!arr->index || index >= arr->index + size)
		return (false);
	// Physically wipes data off by moving what's after over it
	memmove(arr->array + arr->offset + index,
			arr->array + arr->offset + index + size,
			arr->index - index - size);
	arr->index -= size;
	// Tries to resize down
	if (arr->size > arr->min_size && arr->index < arr->size / 4)
		return (ft_dynarray_resize(arr, arr->size / 2));
	return (true);
}

void				*ft_dynarray_purge(
		t_dynarray *arr
)
{
	arr->index = 0;
	if (arr->size > arr->min_size)
		return (ft_dynarray_resize(arr, arr->min_size));
	return (arr);
}

void				ft_dynarray_destroy(
		const t_dynarray * const arr,
		const bool is_alloc
)
{
	free(arr->array);
	if (is_alloc)
		free((void*)arr);
}
