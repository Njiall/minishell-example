/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dynarray_to_str.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/24 16:33:56 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/24 16:38:59 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "dynarray.h"

char					*ft_dynarray_to_str(
		const t_dynarray * const arr
)
{
	char				*str;

	if (!(str = malloc(arr->index + 1)))
		ft_exit(1, ft_strajoin(4, __FILE__, ": ", __func__,
					": No space left on device.\n"), NULL);
	memcpy(str, arr->array + arr->offset, arr->index);
	str[arr->index] = '\0';
	return (str);
}
