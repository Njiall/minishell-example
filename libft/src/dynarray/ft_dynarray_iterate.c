/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dynarray_iterate.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/31 05:02:02 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/31 20:06:16 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "dynarray.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void				*ft_dynarray_iterate(
		const t_dynarray * const arr,
		uint64_t *iterator,
		const uint64_t size
)
{
	if (*iterator * size < arr->index)
	{
		(*iterator)++;
		return (arr->array + arr->offset + (*iterator - 1) * size);
	}
	return (NULL);
}

void				*ft_dynarray_iterate_rev(
		const t_dynarray * const arr,
		uint64_t *iterator,
		const uint64_t size
)
{
	if ((*iterator * size < arr->index) && arr->index != 0)
	{
		(*iterator)++;
		return (arr->array + arr->offset + arr->index - (*iterator * size));
	}
	return (NULL);
}
