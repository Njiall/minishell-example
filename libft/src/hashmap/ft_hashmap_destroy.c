/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hashmap_destroy.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 11:06:34 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/27 11:53:50 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "hashmap.h"
#include "libft.h"

uint32_t			ft_hashmap_destroy_loc(t_hashmap *map)
{
	t_hash_entry	*tmp;
	t_hash_entry	*e;
	uint32_t		i;
	uint32_t		r;

	i = ~0u;
	r = 0;
	while (++i < 0x10000)
	{
		e = map->table[i];
		while (e)
		{
			tmp = e->next;
			free((void*)e->key);
			free((void*)e);
			e = tmp;
			++r;
		}
	}
	++r;
	return (r);
}

uint32_t			ft_hashmap_destroy(t_hashmap *map)
{
	t_hash_entry	*tmp;
	t_hash_entry	*e;
	uint32_t		i;
	uint32_t		r;

	i = ~0u;
	r = 0;
	while (++i < 0x10000)
	{
		e = map->table[i];
		while (e)
		{
			tmp = e->next;
			free((void*)e->key);
			free((void*)e);
			e = tmp;
			++r;
		}
	}
	free(map);
	++r;
	return (r);
}
