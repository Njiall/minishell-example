/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hashmap_hash.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/31 05:07:02 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/27 13:02:45 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "hashmap.h"

/*
** This is a k=33 implentation of the hashing function called 'djb2'
** 		This is a simple but efficient hashing function with harmonious
** 		repartition and few collisions for what it is.
** This is a string based non-cryptographic function which is good for this
** 		kind of use case.
*/

t_hash_type			ft_hash_djb2(
		const void *obj,
		const size_t len
)
{
	const uint8_t	*s;
	uint16_t		hash;
	size_t			i;

	s = (const uint8_t*)obj;
	hash = 5381;
	i = 0;
	while (i < len)
		hash = ((hash << 5) + hash) + s[i++];
	return (hash);
}

t_hash_type			ft_hash_djb2a(
		const void *obj,
		const size_t len
)
{
	const uint8_t	*s;
	uint16_t		hash;
	size_t			i;

	s = (const uint8_t*)obj;
	hash = 5381;
	i = 0;
	while (i < len)
		hash = ((hash << 5) + hash) ^ s[i++];
	return ((uint16_t)hash);
}

/* hash = FNV_offset_basis */
/*    for each byte_of_data to be hashed */
/*        hash = hash XOR byte_of_data */
/*        hash = hash × FNV_prime */
/*    return hash */

t_hash_type			ft_hash_fnv1a(
		const void *obj,
		const size_t len
)
{
	const char		*s;
	uint32_t		hash;
	size_t			i;

	s = (const char *)obj;
	hash = 2166136261;
	i = 0;
	while (i < len)
		hash = (hash ^ s[i++]) * 16777619;
	return (hash);
}
