/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hashmap_manipulation.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/31 05:07:31 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/30 12:44:12 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "hashmap.h"
#include "libft.h"

/*
** Create an entry to use in the hashmap.
*/

static inline t_hash_entry		*ft_hash_entry_create(
		const void * const key,
		const void * const value,
		const size_t len
)
{
	t_hash_entry				*e;

	if (!(e = (t_hash_entry*)malloc(sizeof(t_hash_entry))))
		return (NULL);
	*e = (t_hash_entry){
		.key = (uint8_t*)ft_strdup((const char*)(key)),
		.value = value
	};
	return (e);
}

/*
** Returns the value of an entry in the hashmap for a given key.
** If the key doesn't match a Null pointer is returned.
*/

bool							ft_hashmap_get(
		t_hashmap * const map,
		const void *const key,
		const size_t len,
		const void ** const value
)
{
	t_hash_entry				*entry;

	entry = map->table[map->hash_function(key, len)];
	while (entry && ft_strncmp((const char*)entry->key, (const char*)key, len))
		entry = entry->next;
	if (entry)
	{
		if (value)
			*value = entry->value;
		return (true);
	}
	return (false);
}

/*
** Sets the value of an entry given a key.
** If the entry doesn't exist it's created and then set.
** If the entry already exists then it replaces the previous value
** 	by the new one and returns the old one.
** This will need to be refactored.
*/

const void						*ft_hashmap_set(
		t_hashmap * const map,
		const void * const key,
		const size_t len,
		const void * const value
)
{
	uint16_t					hash;
	t_hash_entry				*hd;
	t_hash_entry				*prev;
	const void *				old_value;

	hash = map->hash_function(key, len);
	hd = map->table[hash];
	prev = NULL;
	while (hd && ft_strncmp((const char*)hd->key, (const char*)key, len))
	{
		prev = hd;
		hd = hd->next;
	}
	old_value = NULL;
	if (hd == NULL && ++map->length)
		if (prev == NULL)
			map->table[hash] = ft_hash_entry_create(key, value, len);
		else
			prev->next = ft_hash_entry_create(key, value, len);
	else
	{
		old_value = hd->value;
		hd->value = (void*)value;
	}
	return (old_value);
}

/*
** Removes an entry from the hashmap and the returns the value that was stored.
*/

const void						*ft_hashmap_remove(
		t_hashmap * const map,
		const void * const key,
		const size_t len
)
{
	uint16_t					hash;
	t_hash_entry				*e;
	t_hash_entry				*p;
	const void					*old_value;

	hash = map->hash_function(key, len);
	e = map->table[hash];
	p = NULL;
	while (e && ft_strcmp((const char*)((void*)e->key), (const char*)(key)))
	{
		p = e;
		e = e->next;
	}
	if (!e)
		return (NULL);
	if (!p)
		map->table[hash] = e->next;
	else
		p->next = e->next;
	old_value = e->value;
	map->length--;
	free((void*)e->key);
	free(e);
	return (old_value);
}

t_hash_entry					*ft_hashmap_iterate(
		const t_hashmap * const map,
		uint32_t *i,
		uint32_t *j
)
{
	uint32_t					k;
	t_hash_entry				*entry;

	while (*i < 0x10000)
	{
		if (map->table[*i])
		{
			k = 0;
			entry = map->table[*i];
			while (entry && k < *j && ++k)
				entry = entry->next;
			*j += 1;
			if (entry)
				return (entry);
		}
		*i += 1;
		*j = 0;
	}
	return (NULL);
}
