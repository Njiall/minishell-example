/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/17 01:44:53 by mbeilles          #+#    #+#             */
/*   Updated: 2020/09/17 05:50:12 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "dynstring.h"
#include "libft.h"

t_dynstring		dynstring_empty(void)
{
	t_dynstring_internal	*new;

	if (!(new = malloc(sizeof(t_dynstring_internal) + 1)))
		return (NULL);
	*new = (t_dynstring_internal){};
	new->string[0] = '\0';
	return (new->string);
}

t_dynstring		dynstring_create(
		const size_t len
)
{
	t_dynstring_internal	*new;

	if (!(new = malloc(sizeof(t_dynstring_internal) + len + 1)))
		return (NULL);
	*new = (t_dynstring_internal){
		.size = len + 1
	};
	new->string[0] = '\0';
	return (new->string);
}

t_dynstring		dynstring_duplicate(
		const t_dynstring string
)
{
	t_dynstring_internal	*dst;
	t_dynstring_internal	*src;

	src = (t_dynstring_internal *)(string - sizeof(dst->size));
	if (!(dst = malloc(sizeof(t_dynstring_internal) + src->size)))
		return (NULL);
	*dst = (t_dynstring_internal){
		.size = src->size
	};
	ft_memcpy(dst->string, string, dst->size);
	return (dst->string);
}

t_dynstring		dynstring_from(
		const t_dynstring_any string
)
{
	t_dynstring_internal	*new;
	size_t					len;

	len = ft_strlen(string);
	if (!(new = malloc(sizeof(t_dynstring_internal) + len + 1)))
		return (NULL);
	*new = (t_dynstring_internal){
		.size = len + 1
	};
	ft_memcpy(new->string, string, len);
	new->string[len] = '\0';
	return (new->string);
}

t_dynstring		dynstring_from_len(
		const char * const string,
		const size_t len
)
{
	t_dynstring_internal	*new;

	if (!(new = malloc(sizeof(t_dynstring_internal) + len + 1)))
		return (NULL);
	*new = (t_dynstring_internal){
		.size = len
	};
	ft_strncpy(new->string, string, len);
	new->string[len] = '\0';
	return (new->string);
}
