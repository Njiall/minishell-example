/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/07 18:02:26 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/07 19:00:15 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static inline size_t	*ft_preproc(
		const char *needle,
		const size_t nlen
)
{
	static size_t		table[256];
	size_t				i;

	i = SIZE_MAX;
	while (++i < 256)
		table[i] = nlen;
	i = nlen;
	while (i > 0)
	{
		table[(unsigned char)needle[i]] = i;
		--i;
	}
	return (table);
}

const char				*ft_strrstr(
		const char *haystack,
		const char *needle
)
{
	size_t				*table;
	size_t				offset;
	size_t				hlen;
	size_t				nlen;
	size_t				i;

	hlen = ft_strlen(haystack);
	nlen = ft_strlen(needle);
	table = ft_preproc(needle, nlen);
	offset = 1;
	while (nlen && offset <= hlen)
	{
		i = 0;
		while (haystack[hlen - offset + i] == needle[i])
			if (i >= nlen - 1)
				return (haystack + hlen - offset);
			else
				++i;
		offset += table[(unsigned char)haystack[hlen - offset]];
	}
	return (NULL);
}
