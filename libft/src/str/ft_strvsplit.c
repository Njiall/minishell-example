/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strvsplit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/18 00:29:31 by mbeilles          #+#    #+#             */
/*   Updated: 2020/09/18 02:02:46 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static inline size_t	_count_words(
		const char * str,
		const char * const pattern
)
{
	bool			prev;
	bool			next;
	size_t			count;

	prev = false; // is in word
	count = 0;
	while (*str)
	{
		next = ft_strchr(pattern, *str); // is in pattern
		if (!prev && !next)
			count++;
		prev = !next;
		str++;
	}
	return (count);
}

// It will split the string and trim the strings.
// It's not space efficient but it's really fast.
char					**ft_strvsplit(
		const char * const str,
		const char * const pattern
)
{
	size_t			len;
	size_t			count;
	char			**split;
	char			*line;
	bool			state[2];

	len = ft_strlen(str);
	count = _count_words(str, pattern) + 1;
	if (!len || !(split = malloc(sizeof(char *) * count + len + 1)))
		return (NULL);
	ft_memcpy((char*)split + sizeof(char *) * count, str, len + 1);
	line = (char*)split + sizeof(char *) * count;
	state[0] = false;
	count = 0;
	while (*line && ((state[1] = ft_strchr(pattern, *line)) | 1))
	{
		if (!state[0] && !state[1])
			split[count++] = line;
		if (state[1])
			*line = '\0';
		state[0] = !state[1];
		line++;
	}
	split[count] = NULL;
	return (split);
}
