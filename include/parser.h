#pragma once

// Here is the parser I will define the BNF
// It's rather primitive for a shell but oh well.
//
// commandline ::= list
//           |  list ";"
//           |  list "&"
// 
// list     ::=  conditional
//           |   list ";" conditional
//           |   list "&" conditional
// 
// conditional ::=  pipeline
//           |   conditional "&&" pipeline
//           |   conditional "||" pipeline
// 
// pipeline ::=  command
//           |   pipeline "|" command
// 
// command  ::=  word
//           |   redirection
//           |   command word
//           |   command redirection
// 
// redirection  ::=  redirectionop filename
// redirectionop  ::=  "<"  |  ">"

#include "command.h"

t_status parse(char *input, t_cmd *cmd);
