#pragma once

#include <stddef.h>

typedef enum {

  // This is default value, so it means an error
  TOK_NONE,
  // Token to signify the end of the stream
  TOK_END,
  // Actual tokens
  TOK_WORD,
  // Operators
  TOK_ASSIGN,
  TOK_STR,
  TOK_VARIABLE,
  TOK_SCOL,
  TOK_PIPE,
  TOK_REDIR,

  // Bounding value
  TOK_LEN,

} t_tok_type;

typedef enum {
  REDIR_ERROR,
  RREDIR,
  RREDIR_APPEND,
  LREDIR,
  LREDIR_APPEND,
  REDIR_RW,
  // Boundary
  REDIR_LEN,
} t_redir_type;

typedef union {
  // Default member
  struct {
    t_tok_type type;
    char *text;
    size_t len;
  };
  struct {
    t_tok_type type;
    char *text;
    size_t len;

    t_redir_type op;
  } redir;
  // We don't need much more but just to be safe
} t_token;

// Token states defaults, to extend in impl.
typedef enum {
  TS_NONE,
  TS_END,
  TS_INIT,
  TS_SAFE_RANGE,
} t_tok_state;

// Token state machine context
typedef struct {
  char *input;
  char *data;

  t_tok_state prev;
  t_tok_state next;

} t_tok_stream;

t_token tokenize(t_tok_stream *iter);
