#pragma once

#include <stdbool.h>
#include <stdint.h>

#include <stdio.h>
#include <stdlib.h>

typedef struct {
  char *text;
  uint32_t code;
  bool valid;
} t_status;

#include "hashmap.h"

typedef struct {

  // Variables
  t_hashmap env;
  t_hashmap local;
  char *prompt;

} t_shell;

extern t_shell g_shell;
